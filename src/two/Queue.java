package two;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue extends ArrayList<String> {
	private Lock queueLock;
	private Condition sufCondition;
	
	public Queue(){
		queueLock = new ReentrantLock();
		sufCondition = queueLock.newCondition();
	}
	public void enqueue(String str) throws InterruptedException{
		queueLock.lock();
		try{
			while (this.size() >= 10){
				sufCondition.await();
			}
			add(str);
			sufCondition.signalAll();
		}finally{
			queueLock.unlock();
		}
		
	}
	public String dequeue() throws InterruptedException{
		String out;
		queueLock.lock();
		try{
			while (this.isEmpty()){
				sufCondition.await();
			}
			out = remove(0);
			sufCondition.signalAll();
		}finally{
			queueLock.unlock();
		}
		return out;
	}

}
