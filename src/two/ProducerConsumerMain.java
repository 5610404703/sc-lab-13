package two;
public class ProducerConsumerMain {

	public static void main(String[] args) {
		
		Queue queue = new Queue();
		
		Producer producer = new Producer(queue);
		Consumer consumer = new Consumer(queue);
		
		Thread producerThread = new Thread(producer);
		Thread consumerThread = new Thread(consumer);
		
		producerThread.start();
		consumerThread.start();
		

	}

}
