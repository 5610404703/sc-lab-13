package one;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws Exception {
		 List<String> q = Collections.synchronizedList(new LinkedList<String>());
	 
	      Thread p1 = new Thread(new PrepareProduction(q));
	      Thread c1 = new Thread(new DoProduction(q));
	      Thread c2 = new Thread(new DoProduction(q));
	      Thread c3 = new Thread(new DoProduction(q));
	      Thread c4 = new Thread(new DoProduction(q));
	      Thread c5 = new Thread(new DoProduction(q));
	    
	       
	      p1.start();
	      c1.start();
	      c2.start();
	      c3.start();
	      c4.start();
	      c5.start();
	      
	      c1.join();
	      c2.join();
	      c3.join();
	      c4.join();
	      c5.join();
	      
	      System.out.println("Done.");
	   }
}
